#!/bin/sh

for app in common schedule accounting; do
  ./manage.py graph_models -g -o docs/$app.png $app
done
