from datetime import timedelta, datetime

from django.db import models
from recurrence.fields import RecurrenceField

from ratrace.common.models import (
    Employee,
    CreatedModifiedModel,
    Department,
    FunctionGroup,
)


class Wheel(CreatedModifiedModel):
    """The "wheel of duty"  for a certain kind of work that ist available for scheduling in a department.

    This could be: daily duty, weekend on-call duty, standby duty, late shift, etc.
    """

    name = models.CharField(max_length=255)
    shortname = models.CharField(max_length=15)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    function_group = models.ForeignKey(FunctionGroup, on_delete=models.CASCADE)

    # describes the rhythm, on which days these slots have to be available:
    days = RecurrenceField()

    start = models.TimeField()
    end = models.TimeField()

    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def hours(self) -> timedelta:
        return datetime.combine(datetime.today(), self.end) - datetime.combine(
            datetime.today(), self.start
        )
