from datetime import datetime

from composite_field import CompositeField
from django.db import models


class TimeRangeField(CompositeField):
    start = models.TimeField()
    end = models.TimeField()

    def duration(self):
        return datetime.combine(datetime.today(), self.end) - datetime.combine(
            datetime.today(), self.start
        )
