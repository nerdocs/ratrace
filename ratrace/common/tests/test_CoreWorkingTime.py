from datetime import time, datetime

import pytest

from ratrace.common.models import CoreWorkingTimeBlock, CoreWorkingTime, User


@pytest.fixture
def employee():
    user = User.objects.create(
        username="test",
        email="test@example.org",
        password="secret",
    )
    return user.employee


def create_time_block(day, start, end, cwt):
    """factory for creating a CoreWorkingTimeBlock"""

    block = CoreWorkingTimeBlock(
        day_of_week=day,
        start=start,
        end=end,
        core_working_time=cwt,
    )
    block.save()
    return block


@pytest.fixture
def default_core_working_time(employee):
    wt = CoreWorkingTime(
        weekly_working_hours=7,
        employee=employee,
        valid_start=datetime.today(),
        employment_percentage=1.0,
    )
    wt.save()
    return wt


@pytest.mark.django_db
def test_days_per_week_calc_distinct(employee, default_core_working_time):
    monday_8_12 = create_time_block(
        CoreWorkingTimeBlock.DaysOfWeek.MON,
        time(8, 0),
        time(12, 0),
        cwt=default_core_working_time,
    )
    monday_16_19 = create_time_block(
        CoreWorkingTimeBlock.DaysOfWeek.MON,
        time(16, 0),
        time(19, 0),
        cwt=default_core_working_time,
    )
    assert default_core_working_time.days_per_week == 1


@pytest.mark.django_db
def test_days_per_week_calc(employee, default_core_working_time):
    monday_8_12 = create_time_block(
        CoreWorkingTimeBlock.DaysOfWeek.MON,
        time(8, 0),
        time(12, 0),
        cwt=default_core_working_time,
    )
    tuesday_16_19 = create_time_block(
        CoreWorkingTimeBlock.DaysOfWeek.TUE,
        time(16, 0),
        time(19, 0),
        cwt=default_core_working_time,
    )
    assert default_core_working_time.days_per_week == 2
