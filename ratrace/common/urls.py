from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView
from django.urls import path
from django.views.generic import TemplateView

from . import views
from .views import (
    ClockInOutView,
    ProfileView,
    CompanyAddView,
    CompanyListView,
    DepartmentListView,
    DepartmentAddView,
)


app_name = "common"

urlpatterns = [
    path("", TemplateView.as_view(template_name="common/index.html"), name="index"),
    path(
        "settings/",
        TemplateView.as_view(template_name="common/settings.html"),
        name="settings",
    ),
    path("clock-in-out", ClockInOutView.as_view(), name="clockinout"),
    path("user/login", LoginView.as_view(), name="login"),
    path("user/profile/<user_id>", ProfileView.as_view(), name="profile"),
    # companies
    # TODO: company-add in companies
    path("companies/", CompanyListView.as_view(), name="company-list"),
    path(
        "companies/<int:pk>/", views.CompanyDetailView.as_view(), name="company-detail"
    ),
    path("companies/add", CompanyAddView.as_view(), name="company-add"),
    path(
        "companies/<int:pk>/update/",
        views.CompanyUpdateView.as_view(),
        name="company-update",
    ),
    path(
        "companies/<int:pk>/delete/",
        views.CompanyDeleteView.as_view(),
        name="company-delete",
    ),
    path(
        "companies/<int:pk>/select",
        views.CompanySelectListView.as_view(),
        name="company-select",
    ),
    # Departments
    path("departments/", DepartmentListView.as_view(), name="departments-list"),
    path(
        "departments/<int:pk>/",
        views.DepartmentDetailView.as_view(),
        name="departments-detail",
    ),
    path("departments/add", DepartmentAddView.as_view(), name="departments-add"),
    path(
        "departments/<int:pk>/update/",
        views.DepartmentUpdateView.as_view(),
        name="departments-update",
    ),
    path(
        "departments/<int:pk>/delete/",
        views.DepartmentDeleteView.as_view(),
        name="departments-delete",
    ),
    # FunctionGroups
    path(
        "functiongroups/",
        views.FunctionGroupListView.as_view(),
        name="functiongroups-list",
    ),
    path(
        "functiongroups/<int:pk>/",
        views.FunctionGroupDetailView.as_view(),
        name="functiongroups-detail",
    ),
    path(
        "functiongroups/add",
        views.FunctionGroupAddView.as_view(),
        name="functiongroups-add",
    ),
    path(
        "functiongroups/<int:pk>/update/",
        views.FunctionGroupUpdateView.as_view(),
        name="functiongroups-update",
    ),
    path(
        "functiongroups/<int:pk>/delete/",
        views.FunctionGroupDeleteView.as_view(),
        name="functiongroups-delete",
    ),
    # Functions
    path(
        "functions/",
        views.FunctionListView.as_view(),
        name="functions-list",
    ),
    path(
        "functions/<int:pk>/",
        views.FunctionDetailView.as_view(),
        name="functions-detail",
    ),
    path(
        "functions/add",
        views.FunctionAddView.as_view(),
        name="functions-add",
    ),
    path(
        "functions/<int:pk>/update/",
        views.FunctionUpdateView.as_view(),
        name="functions-update",
    ),
    path(
        "functions/<int:pk>/delete/",
        views.FunctionDeleteView.as_view(),
        name="functions-delete",
    ),
    # Positions
    path(
        "positions/",
        views.PositionListView.as_view(),
        name="positions-list",
    ),
    path(
        "positions/<int:pk>/",
        views.PositionDetailView.as_view(),
        name="positions-detail",
    ),
    path(
        "positions/add",
        views.PositionAddView.as_view(),
        name="positions-add",
    ),
    path(
        "positions/<int:pk>/update/",
        views.PositionUpdateView.as_view(),
        name="positions-update",
    ),
    path(
        "positions/<int:pk>/delete/",
        views.PositionDeleteView.as_view(),
        name="positions-delete",
    ),
# EmployeeTypes
    path(
        "employeetypes/",
        views.EmployeeTypeListView.as_view(),
        name="employeetypes-list",
    ),
    path(
        "employeetypes/<int:pk>/",
        views.EmployeeTypeDetailView.as_view(),
        name="employeetypes-detail",
    ),
    path(
        "employeetypes/add",
        views.EmployeeTypeAddView.as_view(),
        name="employeetypes-add",
    ),
    path(
        "employeetypes/<int:pk>/update/",
        views.EmployeeTypeUpdateView.as_view(),
        name="employeetypes-update",
    ),
    path(
        "employeetypes/<int:pk>/delete/",
        views.EmployeeTypeDeleteView.as_view(),
        name="employeetypes-delete",
    ),
# Employees
    path(
        "employees/",
        views.EmployeeListView.as_view(),
        name="employees-list",
    ),
    path(
        "employees/<int:pk>/",
        views.EmployeeDetailView.as_view(),
        name="employees-detail",
    ),
    path(
        "employees/add",
        views.EmployeeAddView.as_view(),
        name="employees-add",
    ),
    path(
        "employees/<int:pk>/update/",
        views.EmployeeUpdateView.as_view(),
        name="employees-update",
    ),
    path(
        "employees/<int:pk>/delete/",
        views.EmployeeDeleteView.as_view(),
        name="employees-delete",
    ),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
