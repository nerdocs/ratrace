from django.contrib import admin
from django.contrib.auth import get_user_model

User = get_user_model()

from ratrace.common.models import (
    Company,
    VacationType,
    CoreWorkingTime,
    Employee,
    EmployeeType,
    Vacation,
    VacationBalance,
    CoreWorkingTimeBlock,
    Department,
    FunctionGroup,
    Position,
    Function,
)
from ratrace.schedule.models import Wheel


@admin.register(VacationType)
class VacationTypeAdmin(admin.ModelAdmin):
    ordering = ["weight"]


class UserInline(admin.StackedInline):
    model = User


class CoreWorkingTimeInline(admin.StackedInline):
    model = CoreWorkingTime
    extra = 1
    # fieldsets = (
    #     (None, {"fields": ("monday_start", "monday_end", "tuesday_start")}),
    #     ("Valididy", {"fields": ("valid_start", "valid_end")}),
    # )


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    inlines = [CoreWorkingTimeInline]


admin.site.register(Company)
admin.site.register(Department)
admin.site.register(FunctionGroup)
admin.site.register(Function)
admin.site.register(Position)
admin.site.register(Wheel)
admin.site.register(EmployeeType)
admin.site.register(Vacation)
admin.site.register(VacationBalance)


@admin.register(CoreWorkingTimeBlock)
class WorkingTimeBlockAdmin(admin.ModelAdmin):
    # list_filter = ["coreworkingtime_set.first()"]
    pass


admin.site.register(CoreWorkingTime)
