from decimal import Decimal

import enumfields
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import (
    MinValueValidator,
    MaxValueValidator,
)
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField
from enumfields import Enum, EnumField

User = get_user_model()


# borrowed from MedUX
class CreatedModifiedModel(models.Model):
    """A simple mixin for model classes that need to have created/modified fields."""

    created = models.DateTimeField(editable=False, default=timezone.now)
    modified = models.DateTimeField(editable=False, auto_now=True)

    class Meta:
        abstract = True


class Company(CreatedModifiedModel):
    name = models.CharField(max_length=255)
    logo = models.ImageField(upload_to="companylogos/", blank=True, null=True)
    address = models.CharField(_("Street"), max_length=256)
    city = models.CharField(_("City"), max_length=64)
    country = CountryField(_("Country"), default="Austria")
    zip_code = models.CharField(_("ZIP"), max_length=7)
    contact_tel = models.CharField(_("Phone"), max_length=15)
    contact_fax = models.CharField(_("Fax"), max_length=20, blank=True)
    contact_email = models.EmailField(_("E-Mail"))
    website = models.URLField(_("Website"), max_length=200, blank=True)

    # departments
    # employees
    # coreworkingtime = models.ForeignKey(CoreWorkingTimeBlock, on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class Department(CreatedModifiedModel):
    """TODO: Maybe find a better name. departments are not a good name for GPs, etc."""

    name = models.CharField(max_length=255)
    active = models.BooleanField(default=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, null=True, related_name="department"
    )

    def __str__(self):
        return self.name


class FunctionGroup(CreatedModifiedModel):
    """Used for grouping/ordering of Functions/Slots across departments

    e.g.
    * Endoscopy: Endoscopy I, Endoscopy II, Bronchoscopy have the function group "Endoscopy"
    """

    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=15, blank=True, unique=True)

    def __str__(self):
        return self.name


class Position(CreatedModifiedModel):
    # TODO: we could delete this - this is what Department is for
    """A position in a department where some work is to be done.

    A Position must be occupied for a certain amount of hours per week, e.g. Mo-Fr 9:00-15:00
    """

    name = models.CharField(max_length=255)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name="position_department")

    def __str__(self):
        return self.name


class Function(CreatedModifiedModel):
    """A role/function that any employee may take in a department, according to his/her status/training.

    This could be:
    * Laboratory, Endoscopy, ambulance etc.
    * Reception, Assistant, OP, etc.

    This is needed, as e.g. only nurses with "surgery" training can take slots in the surgery schedule.
    """

    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=15, blank=True, unique=True)
    group = models.ForeignKey(FunctionGroup, on_delete=models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class EmployeeType(CreatedModifiedModel):
    """Type of employment

    e.g. MTA, etc."""

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Employee(CreatedModifiedModel):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    functions = models.ManyToManyField(
        Function, blank=True, related_name="employee_function"
    )
    type = models.ForeignKey(
        EmployeeType, on_delete=models.PROTECT, null=True, blank=True
    , related_name="employee_type")
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, null=True, related_name="employee_company"
    )
    department = models.ManyToManyField(Department, related_name="employee_department")
    position = models.ManyToManyField(Position, related_name="employee_position")
    # FIXME: this can be changed over time too. Should be in the contract/CoreWorkingTime
    holiday_entitlement_hours = models.PositiveSmallIntegerField(
        help_text=_("Hours of holidays per year."), default=0
    )

    def __str__(self):
        return str(self.user)

    def vacations_consumed(self) -> int:
        # TODO: calculate correctly: only this year
        sum = 0
        for vacation in self.vacations.values():  # type: Vacation
            sum += len(vacation)
        return sum

    def vacation_balance(self) -> int:
        """:return: hours of current vacation balance"""

        return self.vacation_balances.order_by("date").last().balance


# FIXME: This part caused a Related name error "User has no Employee" - Accoding to this https://stackoverflow.com/questions/63211265/relatedobjectdoesnotexist-user-has-no-profile the super.save() method is missing

# @receiver(post_save, sender=User)
# def create_employee(sender, instance: User, created, **kwargs):
#     """Create Employee for every new User."""
#
#     if created:
#         Employee.objects.create(user=instance)
#     instance.employee.save()


class VacationType(models.Model):
    """Type of vacations: normal, special leave, etc."""

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    weight = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.name


class Vacation(CreatedModifiedModel):
    """A vacation entry of one employee"""

    employee = models.ForeignKey(
        Employee, on_delete=models.CASCADE, related_name="vacations"
    )
    type = models.ForeignKey(VacationType, default=1, on_delete=models.SET_DEFAULT)
    start = models.DateField()
    end = models.DateField()

    @property
    def days(self) -> int:
        """The length of the vacation in days"""
        return (self.end - self.start).days

    def __len__(self):
        return self.days * 24

    def clean(self):
        if self.start >= self.end:
            raise ValidationError(_("'start' can not occur timely after 'end'."))


class VacationBalance(CreatedModifiedModel):
    """Current "balance" of vacation of an employee

    This should not be necessary to add manually, as it is calculated and saved
    automatically when consuming vacation
    However, At the beginning of employing someone, it may be helpful to create the "offset" of his balance.
    """

    employee = models.ForeignKey(
        Employee, on_delete=models.CASCADE, related_name="vacation_balances"
    )
    date = models.DateField()
    balance = models.PositiveIntegerField(default=0)

    # TODO: creating balances must be done automatically by a "cron"job:
    #  when currently "on vacations" today, reduce employee's vacation_balance by 24h (8 hours?)

    # TODO: make sure that any employee gets a VacationBalance at creation, OK if 0


class CoreWorkingTimeBlock(models.Model):
    """A block at one week day with working times.

    This is needed as basic data for an employment.
    Many of these blocks form a CoreWorkingTime.
    """

    class DaysOfWeek(Enum):
        MON = 1
        TUE = 2
        WED = 3
        THU = 4
        FRI = 5
        SAT = 6
        SUN = 7

        class Labels:
            MON = _("Monday")
            TUE = _("Tuesday")
            WED = _("Wednesday")
            THU = _("Thursday")
            FRI = _("Friday")
            SAT = _("Saturday")
            SUN = _("Sunday")

    day_of_week = EnumField(DaysOfWeek)
    start = models.TimeField()
    end = models.TimeField()
    core_working_time = models.ForeignKey(
        "CoreWorkingTime", on_delete=models.CASCADE, related_name="blocks"
    )
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True)

    def __str__(self):
        # TODO: output something more meaningful
        user = self.core_working_time.employee.user  # type: User

        # noinspection PyUnresolvedReferences
        return (
            f"{user.first_name} {user.last_name}: "
            f"{self.day_of_week.label}: {self.start.hour:02d}:{self.start.minute:02d} - "
            f"{self.end.hour:02d}:{self.end.minute:02d}"
        )

    @property
    def employee(self):
        return self.core_working_time.employee


class CoreWorkingTime(CreatedModifiedModel):
    """represents a contract for working hours with an employee

    This could change over time, so it is possible to create new CoreWorkingTimes
    with a different start_date.
    """

    weekly_working_hours = models.DecimalField(
        default=settings.FULL_EMPLOYMENT_HOURS, decimal_places=2, max_digits=5
    )
    employment_percentage = models.DecimalField(
        default=1.0,
        max_digits=3,
        decimal_places=2,
        validators=[MinValueValidator(0.0), MaxValueValidator(1.0)],
    )
    # FIXME: These are no corworkingtimes but only dates (like date of employment
    valid_start = models.DateField()
    valid_end = models.DateField(blank=True, null=True)

    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)

    def __str__(self):
        return str([str(block) for block in self.blocks.order_by("day_of_week")])

    @property
    def days_per_week(self):
        return self.blocks.values_list("day_of_week").distinct().count()
        # return self.blocks.distinct("day_of_week").count()

    def daily_working_time_avg(self) -> Decimal:
        """average daily working hours"""
        return self.weekly_working_hours / self.days_per_week

    def monthly_working_time(self) -> Decimal:
        """Returns the calculated montly working hours"""
        return self.weekly_working_hours * settings.MONTH_FACTOR

    def clear(self):
        if self.weekly_working_hours != self.employment_percentage * Decimal(
            settings.FULL_EMPLOYMENT_HOURS
        ):
            raise ValidationError(
                _("working hours and percentage must match each other!")
            )

    # def weekly_working_time(self):
    #     delta = timedelta()
    #     for block in self.blocks.all():
    #         delta += datetime.combine(datetime.today() + block.end) - datetime.combine(
    #             datetime.today() + block.start
    #         )
    #     minutes = delta.seconds / 60
    #     return minutes // 60


class ClockingTimeStamp(CreatedModifiedModel):
    class Logging(Enum):
        IN = "in"
        OUT = "out"

        class Labels:
            IN = _("logged in")
            OUT = _("logged out")

    # created/modified is automatically included from CreatedModifiedModel
    employee = models.ForeignKey(
        Employee, on_delete=models.CASCADE, verbose_name=_("User")
    )

    # when the user has clocked in/out
    timestamp = models.DateTimeField()
    logging = enumfields.EnumField(Logging, max_length=255)
    comment = models.CharField(max_length=255, blank=True)
    recorded_by = models.ForeignKey(
        Employee,
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
        help_text=_("The person that recorded the clocking"),
    )

    def __str__(self):
        """returns a string that describes when the employee was logged in/out."""
        return _("{logging} at {timestamp}").format(
            logging=self.logging, timestamp=self.clocking_time
        )
