from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    DeleteView,
    UpdateView,
)
from django.utils.translation import gettext_lazy as _
from ratrace.common.models import Function


class FunctionListView(ListView):
    model = Function
    fields = "__all__"
    template_name = "common/function_list.html"
    queryset = Function.objects.all()
    context_object_name = "functions"


class FunctionDetailView(DetailView):
    model = Function


# FIXME: Save and successurl not working on save or update


class FunctionAddView(CreateView):
    template_name = "common/function_form.html"
    model = Function
    fields = "__all__"
    success_url = "/functions"
    extra_context = {"title": _("Add function")}


class FunctionUpdateView(UpdateView):
    model: Function
    fields = "__all__"
    template_name = "common/function_form.html"
    queryset = Function.objects.all()
    success_url = "/functions"
    extra_context = {"title": _("Edit function")}


class FunctionDeleteView(DeleteView):
    model = Function
    template_name = "common/confirm_delete.html"
    success_url = "/functions"
