from django.contrib.auth.views import LoginView as DjangoLoginView
from django.http import HttpRequest
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView,
)

from ..forms import ClockInOutForm, CompanyForm
from ..models import ClockingTimeStamp, Company


class ClockInOutView(CreateView):
    model = ClockingTimeStamp
    form_class = ClockInOutForm

    def post(self, request, *args, **kwargs):
        # request.
        super().post(request, *args, **kwargs)


class LoginView(DjangoLoginView):
    success_url = "/user/profile"


class ProfileView(DetailView):
    pass


class CompanyAddView(CreateView):
    model = Company
    form_class = CompanyForm
    success_url = "/companies"
    extra_context = {"title": _("Add company")}


class CompanyDetailView(DetailView):
    model = Company


class CompanyListView(ListView):
    model = Company
    template_name = "common/company_list.html"
    context_object_name = "companies"


class CompanyUpdateView(UpdateView):
    model: Company
    template_name = "common/company_form.html"
    form_class = CompanyForm
    queryset = Company.objects.all()
    success_url = "/companies"
    extra_context = {"title": _("Edit company")}


class CompanyDeleteView(DeleteView):
    model = Company
    template_name = "common/confirm_delete.html"
    success_url = "/companies"


class CompanySelectListView(ListView):
    model = Company
    template_name = "common/company_list_table.html"

    def post(self, request: HttpRequest, pk):
        request.session["current_company_id"] = pk
        return render(request, self.template_name)

    # def get(self, request: HttpRequest, *args, **kwargs):
    #     request.session["current_company_id"] = request.GET["company_id"]
    #     super().get(args, kwargs)
    #     return redirect(reverse("common:company-list"))
