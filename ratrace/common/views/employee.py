from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    DeleteView,
    UpdateView,
)
from django.utils.translation import gettext_lazy as _
from ratrace.common.models import Employee


class EmployeeListView(ListView):
    model = Employee
    fields = "__all__"
    template_name = "common/employee_list.html"
    queryset = Employee.objects.all()
    context_object_name = "employees"


class EmployeeDetailView(DetailView):
    model = Employee


class EmployeeAddView(CreateView):
    template_name = "common/employee_form.html"
    model = Employee
    fields = "__all__"
    success_url = "/employees"
    extra_context = {"title": _("Add Employee")}


class EmployeeUpdateView(UpdateView):
    model: Employee
    fields = "__all__"
    template_name = "common/employee_form.html"
    queryset = Employee.objects.all()
    success_url = "/employees"
    extra_context = {"title": _("Edit Employee")}


class EmployeeDeleteView(DeleteView):
    model = Employee
    template_name = "common/confirm_delete.html"
    success_url = "/employees"
