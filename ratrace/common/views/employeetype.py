from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    DeleteView,
    UpdateView,
)
from django.utils.translation import gettext_lazy as _
from ratrace.common.models import EmployeeType, Employee


class EmployeeTypeListView(ListView):
    model = EmployeeType
    fields = "__all__"
    template_name = "common/employeetype_list.html"
    queryset = EmployeeType.objects.all()
    context_object_name = "employeetypes"


class EmployeeTypeDetailView(DetailView):
    model = EmployeeType


class EmployeeTypeAddView(CreateView):
    template_name = "common/employeetype_form.html"
    model = EmployeeType
    fields = "__all__"
    success_url = "/employeetypes"
    extra_context = {"title": _("Add Employeetype")}

    # TODO: forms.MultipleChoiceField(widget=ChEckbox)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["employee_list"] = Employee.objects.all()
        return context

class EmployeeTypeUpdateView(UpdateView):
    model: EmployeeType
    fields = "__all__"
    template_name = "common/employeetype_form.html"
    queryset = EmployeeType.objects.all()
    success_url = "/employeetypes"
    extra_context = {"title": _("Edit Employeetype")}


class EmployeeTypeDeleteView(DeleteView):
    model = EmployeeType
    template_name = "common/confirm_delete.html"
    success_url = "/employeetypes"
