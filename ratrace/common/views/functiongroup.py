from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    DeleteView,
    UpdateView,
)
from django.utils.translation import gettext_lazy as _
from ratrace.common.models import FunctionGroup


class FunctionGroupListView(ListView):
    model = FunctionGroup
    fields = "__all__"
    template_name = "common/functiongroup_list.html"
    queryset = FunctionGroup.objects.all()
    context_object_name = "functiongroups"


class FunctionGroupDetailView(DetailView):
    model = FunctionGroup


class FunctionGroupAddView(CreateView):
    template_name = "common/functiongroup_form.html"
    model = FunctionGroup
    fields = "__all__"
    success_url = "/functiongroups"
    extra_context = {"title": _("Add functiongroup")}


class FunctionGroupUpdateView(UpdateView):
    model: FunctionGroup
    fields = "__all__"
    template_name = "common/functiongroup_form.html"
    queryset = FunctionGroup.objects.all()
    success_url = "/functiongroups"
    extra_context = {"title": _("Edit functiongroup")}


class FunctionGroupDeleteView(DeleteView):
    model = FunctionGroup
    template_name = "common/confirm_delete.html"
    success_url = "/functiongroups"
