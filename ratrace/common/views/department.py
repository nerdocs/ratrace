from django.urls import reverse
from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    DeleteView,
    UpdateView,
)
from django.utils.translation import gettext_lazy as _
from ratrace.common.models import Department


class DepartmentListView(ListView):
    model = Department
    fields = "__all__"
    template_name = "common/department_list.html"
    queryset = Department.objects.all()
    context_object_name = "departments"


class DepartmentDetailView(DetailView):
    model = Department


class DepartmentAddView(CreateView):
    template_name = "common/department_form.html"
    model = Department
    fields = "__all__"
    success_url = "/departments"
    extra_context = {"title": _("Add department")}

    # def get_success_url(self):
    #     if self.request.POST.get("save"):
    #         return reverse("/departments")
    #     elif self.request.POST.get("save_and_addnew"):
    #         return reverse("/departments/add", kwargs={"pk": self.object.pk})


class DepartmentUpdateView(UpdateView):
    model: Department
    fields = "__all__"
    template_name = "common/department_form.html"
    queryset = Department.objects.all()
    success_url = "/departments"
    extra_context = {"title": _("Edit department")}


class DepartmentDeleteView(DeleteView):
    model = Department
    template_name = "common/confirm_delete.html"
    success_url = "/departments"
