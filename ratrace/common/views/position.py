from django import forms
# from django.forms import forms
from django.http import request
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    DeleteView,
    UpdateView,
)
from django.utils.translation import gettext_lazy as _

from ratrace.common.models import Position, Employee


class PositionListView(ListView):
    model = Position
    fields = "__all__"
    template_name = "common/position_list.html"
    queryset = Position.objects.all()
    context_object_name = "Positions"


class PositionDetailView(DetailView):
    model = Position


# FIXME: Save and successurl not working on save or update


class PositionAddView(CreateView):
    template_name = "common/position_form.html"
    model = Position
    fields = "__all__"
    success_url = "/positions"
    extra_context = {"title": _("Add Position")}

# TODO: forms.MultipleChoiceField(widget=ChEckbox)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["employee_list"] = Employee.objects.all()
        return context


class PositionUpdateView(UpdateView):
    model: Position
    fields = "__all__"
    template_name = "common/position_form.html"
    queryset = Position.objects.all()
    success_url = "/positions"
    extra_context = {"title": _("Edit Position")}


class PositionDeleteView(DeleteView):
    model = Position
    template_name = "common/confirm_delete.html"
    success_url = "/positions"
