from django.http import HttpRequest

from ratrace.common.models import Company


def ratrace_globals(request: HttpRequest):
    """Adds global context that is needed in every view"""

    # TODO: only return companies user has access to
    companies = Company.objects.all()

    current_company = None

    company_id = request.session.get("current_company_id", None)
    if company_id:
        try:
            current_company = Company.objects.get(pk=company_id)
        except Company.DoesNotExist:
            # Reset current_company_id in session
            request.session["current_company_id"] = None

    return {"current_company": current_company, "companies": companies}
