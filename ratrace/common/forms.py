from crispy_forms.bootstrap import (
    PrependedAppendedText,
)
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from django import forms

from .models import ClockingTimeStamp, Company, Position, Employee


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = "__all__"
        widgets = {}


class ClockInOutForm(forms.ModelForm):
    class Meta:
        model = ClockingTimeStamp
        exclude = ("recorded_by",)

    password = forms.CharField(widget=forms.PasswordInput)

    helper = FormHelper()
    helper.form_class = "form-horizontal"
    helper.layout = Layout(
        "employee",
        "password",
        "logging",  # TODO: use RadioButtonGroup
        PrependedAppendedText(  # TODO: create own widget, fix datepicker
            "timestamp",
            "-15min",
            # TODO make buttons here. not possible with PrependedAppendedText, as only safe HTML is allowed.
            "+15min",
        ),
        "comment",
    )


